require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:product) { create(:base_product, taxons: [taxon]) }
  let!(:related_products) { create_list(:base_product, 4, taxons: [taxon]) }
  let(:unrelated_products) { create_list(:base_product, 4, taxons: [other_taxon]) }

  describe "#related_products" do
    subject { product.related_products }

    it "関連商品が存在し、正常であること" do
      is_expected.to eq related_products
    end

    it "関連商品の個数が正しいこと" do
      expect(subject.count).to eq related_products.count
    end

    it "関連しない商品が含まれていないこと" do
      is_expected.not_to include unrelated_products
    end

    it "選択中のメイン商品の関連商品には、メイン商品自身が含まれないこと" do
      is_expected.not_to include product
    end

    it "重複が無いこと" do
      expect(subject.uniq).to eq subject
    end
  end
end
