require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "カテゴリー一覧ページ" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:product) { create(:base_product, taxons: [taxon]) }
    let(:image) { build(:image) }

    before do
      product.images << image if product
      get potepan_category_path(taxon.id)
    end

    it 'カテゴリーページにアクセスできること' do
      expect(response).to have_http_status 200
    end

    it "カテゴリーに紐付く商品が正しく表示されていること" do
      aggregate_failures do
        expect(response.body).to include product.name
        expect(response.body).to include product.price.to_s
        expect(response.body).to include product.display_image.attachment(:product)
      end
    end
  end
end
