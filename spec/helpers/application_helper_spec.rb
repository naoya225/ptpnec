require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titleメソッド" do
    subject { full_title(page_title: title) }

    let(:base_title) { "BIGBAG Store" }

    context "引数にタイトルを渡す" do
      let(:title) { "TestTiltle" }

      it "完全なタイトルを返すこと" do
        expect(subject).to eq "#{title} | #{base_title}"
      end
    end

    context "引数にタイトルを渡さない" do
      let(:title) { "" }

      it "基本のタイトルを返すこと" do
        expect(subject).to eq base_title
      end
    end

    context "引数にnilを渡す" do
      let(:title) { nil }

      it "基本のタイトルを返すこと" do
        expect(subject).to eq base_title
      end
    end

    context "引数無し" do
      it "基本のタイトルを返すこと" do
        expect(full_title).to eq base_title
      end
    end
  end
end
