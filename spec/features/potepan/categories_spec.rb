require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  describe "カテゴリー一覧ページ" do
    let(:taxon) { create(:taxon, parent: taxonomy.root) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:image) { build(:image) }
    let(:page_title) { taxon.name }

    before do
      product.images << image
      visit potepan_category_path(taxon.id)
    end

    scenario "正しいタイトルが表示されること" do
      expect(page).to have_title "#{taxon.name} | BIGBAG Store"
    end

    describe "サイドバーの商品カテゴリー" do
      scenario "正しいカテゴリーの大分類(taxonomy)と小分類(taxon)を表示されていること" do
        within(:css, '.navbar-side-collapse') do
          expect(page).to have_content taxonomy.name
          expect(page).to have_content "#{taxon.name} (#{taxon.all_products.count})"
        end
      end

      scenario "カテゴリー(taxon)をクリックした時正しいカテゴリーページへ遷移すること" do
        within(:css, ".navbar-side-collapse") do
          click_link taxon.name, match: :first
        end
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    describe "商品一覧" do
      let(:other_product) { create(:product, taxons: [other_taxon]) }
      let(:other_taxon) { create(:taxon, parent: other_taxonomy.root) }
      let(:other_taxonomy) { create(:taxonomy) }

      before do
        other_product.images << image
      end

      scenario "カテゴリー(taxon)に紐付いたproductのみ表示すること" do
        expect(page).not_to have_content other_product.name
      end

      scenario "商品名をクリックすると、商品詳細ページへ遷移すること" do
        expect(page).to have_link product.name
        click_link product.name
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
