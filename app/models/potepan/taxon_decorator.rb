module Potepan::TaxonDecorator
  Spree::Taxon.class_eval do
    scope :leaf_taxons, -> { where.not(depth: 0) }
  end

  Spree::Taxon.prepend self
end
