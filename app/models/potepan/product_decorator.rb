module Potepan::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons.leaf_taxons).
      includes(master: [:images, :default_price]).
      distinct.
      where.not(id: id)
  end

  Spree::Product.prepend self
end
