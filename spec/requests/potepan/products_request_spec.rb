require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:product) { create(:base_product, taxons: [taxon]) }

  before do
    get potepan_product_path product.id
  end

  describe "商品詳細" do
    it "商品詳細ページにアクセスできること" do
      expect(response).to have_http_status 200
    end

    it "商品名と価格、商品説明が正しく表示されていること" do
      aggregate_failures do
        expect(response.body).to include product.name
        expect(response.body).to include product.price.to_s
        expect(response.body).to include product.description
      end
    end
  end

  describe "関連商品" do
    subject { product.related_products }

    let!(:related_products) { create_list(:base_product, 4, taxons: [taxon]) }

    it "選択中の中のメイン商品は４件の関連商品をもつ" do
      expect(subject.size).to eq 4
    end

    it "関連商品の情報が正しく表示されていること" do
      subject.each do |related_product|
        within ".productsContent" do
          expect(response.body).to have_content related_product.display_price
          expect(response.body).to have_content related_product.display_image.attachment(:large)
        end
      end
    end
  end
end
