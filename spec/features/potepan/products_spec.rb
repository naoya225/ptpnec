require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:product) { create(:base_product, taxons: [taxon]) }
  let!(:related_products) { create_list(:base_product, 4, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  describe "商品詳細" do
    scenario "商品詳細ページに商品名が３箇所存在すること" do
      expect(page).to have_content product.name, count: 3
    end

    scenario "商品詳細ページに料金が存在すること" do
      expect(page).to have_content product.price.to_s
    end

    scenario "商品詳細ページに商品説明文が存在すること" do
      expect(page).to have_content product.description
    end

    scenario "商品詳細ページにホームページへのリンクが存在すること" do
      within ".breadcrumb" do
        click_link "Home"
      end
      expect(current_path).to eq potepan_path
    end

    scenario "商品が属するカテゴリー(taxon)ページへのリンクが存在すること" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end

    scenario "正しいタイトルが表示されること" do
      expect(page).to have_title "#{product.name} | BIGBAG Store"
    end
  end

  describe "関連商品" do
    subject { product.related_products }

    scenario "関連商品をクリックすると、その商品詳細ページへ遷移すること" do
      within ".productsContent" do
        click_link subject.first.name
        expect(current_path).to eq potepan_product_path(subject.first.id)
      end
    end

    context "関連商品が４件以上存在する場合" do
      let!(:related_products) { create_list(:base_product, 5, taxons: [taxon]) }

      scenario "４件表示すること" do
        expect(page).to have_selector ".productBox", count: 4
      end
    end
  end
end
